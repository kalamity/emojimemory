$(document).ready(function() {
    init();
});

function init() {
    var boxes = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8];

    //Shuffle  background
    function shuffle(boxes) {
        for (let i = boxes.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [boxes[i], boxes[j]] = [boxes[j], boxes[i]];
        }
        console.log("Shuffle Emoji" + boxes);
    }
    shuffle(boxes);

    // Change background image
    function spreadBoxes(boxes) {
        var index = 0;
        $(".box").each(function() {
            $(this).css({
                "background-image": "url(assets/emoji/" + boxes[index] + ".png)"
            });
            index++;
        });
    }
    spreadBoxes(boxes);

    //Add the .hidden class to hide the image
    setTimeout(function() {
        $(".box").each(function() {
            $(this).addClass("hidden");
        });
    }, 900);

    //Remove class hidden and add class selected
    $(".box").each(function() {
        $(this).click(function() {
            if ($(this).hasClass("match") || $(this).hasClass("selected")) {
                return;
            } // for not idding a cards that as already been match with an other
            $(this).removeClass("hidden");
            $(this).addClass("selected");
            compare();
        });
    });

    //Compare Box
    function compare() {
        var cards = $(".selected");
        if (cards.length === 2) {
            if (
                $(cards[0]).css("background-image") ==
                $(cards[1]).css("background-image")
            ) {
                setTimeout(function() {}, 300);
                $(cards).removeClass("selected"); //remove class .selected for the if condition to continue to work
                $(cards).addClass("match"); // add a class to  help for the checkWin function
                checkWin();
            } else {
                setTimeout(function() {
                    $(cards).removeClass("selected");
                    $(cards).addClass("hidden");
                }, 200);
            }
        }
    }

    //Check  if cards are the same
    function checkWin() {
        var pairs = $(".match");
        if (pairs.length === 16) {
            setTimeout(function() {
                $('#youWin').css({ "display": "flex" });
            }, 300);
            stop();
        }
    }

    //Reload the Page on the RESET button
    $("button").click(function() {
        location.reload(true);
    });

    //Chrono
    // initialisation
    var time = 60000; //temps de depart
    var isCountdownOn = false;
    var countElement = document.getElementById("time");
    countElement.innerHTML = time / 1000;
    var intervalID;

    function start() {
        if (!isCountdownOn == true) {
            isCountdownOn = true;
            intervalID = setInterval(countdown, 1000);
        }
    }

    function countdown() {
        time -= 1000;
        countElement.innerHTML = time / 1000;
        if (time == 0) {
            $('#youLose').css({ "display": "flex" });
            stop();
        }
    }

    function stop() {
        clearInterval(intervalID)
    }

    $(document).click(function() {
        start();
    });
}